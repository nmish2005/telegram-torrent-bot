import parseTorrent from 'parse-torrent'

export class TorrentEntity {
    parsedTorrent = ''

    constructor(url) {
        this.parsedTorrent = parseTorrent(url)
    }

    asMagnet() {
        return parseTorrent.toMagnetURI(this.parsedTorrent)
    }
}

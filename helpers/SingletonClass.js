export class SingletonClass {
    static instance = undefined

    constructor() {
        throw new Error('Use Class.getInstance instead of constructor')
    }

    static getInstance() {
        if (!this.instance) {
            this.createInstance()
        }

        return this.instance
    }

    static createInstance() {
        this.instance = {}
    }
}

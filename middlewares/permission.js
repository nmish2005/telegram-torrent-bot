export function permissionMiddleware() {
    const familyMembers = JSON.parse(process.env.FAMILY_MEMBERS),
        allowedUserIds = Object.keys(familyMembers).map(Number)

    return (ctx, next) => {
        if (!allowedUserIds.includes(ctx.from.id)) {
            return
        }

        next()
    }
}

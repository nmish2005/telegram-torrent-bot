import {SingletonClass} from "../helpers/SingletonClass.js";
import TransmissionBinding from 'transmission';

export class TransmissionProvider extends SingletonClass {
    static createInstance() {
        this.instance = new TransmissionBinding()
    }
}

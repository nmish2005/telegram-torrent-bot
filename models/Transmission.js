import {TransmissionProvider} from "../providers/TransmissionProvider.js";

export class Transmission {
    static async getActiveTorrents() {
        return new Promise(resolve => {
            TransmissionProvider.getInstance().active((err, arg) => {
                if (err) {
                    console.log(err)
                }

                console.log(arg)

                resolve(arg.torrents.length ? arg.torrents.map(i => `_${i.name}_ ***Left ${parseInt(i.leftUntilDone)} of ${parseInt(i.desiredAvailable)}***`) : 'Нет активных торрентов')
            })
        });
    }

    static async download(magnetURI, options) {
        return new Promise(resolve => {
            TransmissionProvider.getInstance().addUrl(magnetURI, options, (err, arg) => {
                if (err) {
                    console.log(err)
                }

                resolve(arg)
            })
        });
    }
}

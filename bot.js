import 'dotenv/config'
import {Scenes, session, Telegraf} from 'telegraf'
import {permissionMiddleware} from "./middlewares/permission.js";
import {Transmission} from "./models/Transmission.js";
import {addTorrentScene} from "./scenes/addTorrent.js";

(async () => {
    const bot = new Telegraf(process.env.BOT_TOKEN),
        familyMembers = JSON.parse(process.env.FAMILY_MEMBERS),
        allowedUserIds = Object.keys(familyMembers).map(Number),
        scenes = [addTorrentScene]

    const stage = new Scenes.Stage(scenes);

    bot.use(session())
    bot.use(stage.middleware())
    bot.use(permissionMiddleware())

    bot.start(ctx => {
        ctx.replyWithSticker('CAACAgIAAxkBAAMHYm-2OnkMNnVXE2vR9cWF-BPKwWAAAioDAALPu9QOH_K1GH9lnzAkBA', {
            parse_mode: 'MarkdownV2'
        })
    })

    scenes.forEach(scene => scene.command('/cancel', Scenes.Stage.leave()))

    // bot.on('callback_query', async ctx => {
    //     switch (ctx.update.callback_query.data) {
    //         case 'open_gate':
    //             await eConnection.setDevicePowerState(process.env.GATE_DEVICEID, 'on', 1)
    //
    //             notifyFamily(ctx.from.id, '🔓 поднял ворота')
    //             break
    //
    //         case 'close_gate':
    //             await eConnection.setDevicePowerState(process.env.GATE_DEVICEID, 'on', 2)
    //
    //             notifyFamily(ctx.from.id, '🔒 опустил ворота')
    //     }
    //
    //     ctx.answerCbQuery()
    // })

    bot.command('/list', async ctx => {
        ctx.replyWithMarkdownV2(await Transmission.getActiveTorrents())
    })

    bot.command('/add', Scenes.Stage.enter('ADD_TORRENT_SCENE'))

    bot.launch().then(() => {
        console.log('The bot is running')
    })

    // function notifyFamily(userId, actionTitle) {
    //     const userName = familyMembers[userId.toString()].name
    //
    //     allowedUserIds.forEach(id => {
    //         bot.telegram.sendMessage(id, `***${userName}*** ${actionTitle}`, {
    //             parse_mode: 'MarkdownV2'
    //         })
    //     })
    // }

// Enable graceful stop
    process.once('SIGINT', () => bot.stop('SIGINT'))
    process.once('SIGTERM', () => bot.stop('SIGTERM'))
})()

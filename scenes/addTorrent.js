import {Scenes} from 'telegraf';
import fetch from "node-fetch"
import {TorrentEntity} from "../helpers/TorrentEntity.js";
import {Transmission} from "../models/Transmission.js";

export const addTorrentScene = new Scenes.WizardScene(
    'ADD_TORRENT_SCENE',
    (ctx) => {
        ctx.reply('Отправь мне magnet-ссылку или torrent-файл для скачивания. Используй /cancel для отмены');

        return ctx.wizard.next();
    },
    async (ctx) => {
        if (ctx.message.text) {
            if (/magnet:\?xt=urn:[a-z0-9]+:[a-z0-9]{32}/i.test(ctx.message.text)) {
                ctx.wizard.state.torrent = new TorrentEntity(ctx.message.text)
            } else {
                return ctx.reply('Неправильный формат ссылки')
            }
        } else if (ctx.message.document) {
            const torrentFileLink = (await ctx.telegram.getFileLink(ctx.message.document.file_id)).href,
                torrentFile = await (await fetch(torrentFileLink)).arrayBuffer()

            ctx.wizard.state.torrent = new TorrentEntity(Buffer.from(torrentFile))
        } else {
            return ctx.reply('Принимаются только magnet-ссылки или torrent-файлы')
        }

        const result = await Transmission.download(ctx.wizard.state.torrent.asMagnet(), {
            "download-dir": process.env.DOWNLOAD_DIR
        })

        ctx.replyWithMarkdown('☑️ Торрент ***' + result.name.replaceAll('+', ' ') + '*** добавлен в очередь.');

        return ctx.scene.leave()
    }
);
